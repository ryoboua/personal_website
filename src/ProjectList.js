import React, { Component } from 'react'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import Chip from '@material-ui/core/Chip'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

const projectList = [
    {
        title: 'NotePadApp',
        url: 'http://whoisreggie.ca/notepadapp',
        code: 'https://github.com/ryoboua/notepadapp',
        Details: () => (
            <p className="text-left" >
                <u>Why I built this:</u> I wanted to code in a Nodejs enviroment and solidify my knowledge of javascript and its core concepts.<br/><br/>

                <u>Challenge:</u> Building a responsive fullstack app from the ground up.<br/><br/>

                <u>Major takeaway:</u> As a <a href="https://www.youtube.com/watch?v=YlE1GUCl8fM" target="_blank" rel="noopener noreferrer" >naïve programmer</a>,
                while building the backend, I ran regression tests using Postman every time I made major changes to my code rather than utilizing test cases.<br/>
                As the project grew more complex, this process became cumbersome.
                I’m always pushing myself to find better solutions - so I did some research and learned about test-driven development.
                I was able to experience first hand how TDD dramatically improves and speeds up development.
                You have a lot more confidence in your code when it passes all your test suites everytime you save a single change.
                Building this app ultimately taught me the importance of test cases and frameworks such as Jest, Mocha, and Chai.<br/><br/>
            </p>
        ) 
    },
    {
        title: 'Elevated Protection',
        url: 'https://elevatedprotection.ca',
        code: 'https://github.com/ryoboua/elevated_protection_website',
        Details: () => (
            <p className="text-left" >
                <u>Why I built this:</u> This was a paid freelance project, so I took on this project to gain freelance experience
                - side note: I'm trying to become a <i>digital nomad</i>. Furthermore, I also wanted to see if I could deliver on a client’s vision.
                The client gave me a PDF file of what they wanted the website to look like and I built it using React and I wrote all the CSS by hand because I wanted to improve my CSS.
                <br/><br/>

                <u>Challenge:</u> Making a responsive website. Making sure I was on track with my development goals/schedule.<br/><br/> 
                <u>Major takeaway:</u> Before this project I hadn't hosted any applications in a while.
                Most project were just built locally, I would never really host them.
                As a result, I had to learn how to purchase a domain name as well as route all request to my DigitalOcean droplet.
                I created a small express server to serve the React build.
                </p>
        ) 
    },
    {
        title: 'Awesome WeatherApp',
        url: 'http://whoisreggie.ca/weatherapp',
        code: 'https://github.com/ryoboua/awesome_react_weather_app',
        Details: () => (
            <p className="text-left" >
                <u>Why I built this:</u> Everyone needs a weather app and it’s general knowledge that React and Redux go hand in hand.
                <br/><br/> 
                <u>Challenge:</u> Integrating Redux in a React app<br/><br/> 
                <u>Major takeaway:</u> Passing down a state to children can be annoying sometimes especially if the child is nested by multiple levels 
                and also if that state is used by multiple components.
                So I was able to see the advantages of Redux.
                For my personal projects, I like to use the React Context API because it uses React's native state manager.
            </p>
        ) 
    },
    {
        title: 'Switch Board',
        url: 'http://whoisreggie.ca/switchboard',
        code: 'https://github.com/ryoboua/SwitchBoard',
        Details: () => (
            <p className="text-left" >
                <u>Why I built this:</u> To fully familiarize myself with React.<br/><br/>
                <u>Challenge:</u> This was my first React app.<br/><br/>
                <u>Major takeaway:</u> Understood the fundamental aspects of React: state, props, component lifecycle, virtual DOM, and the concept of elevating the state. 
            </p>
        ) 
    },
    {
        title: 'Personal Website',
        url: '',
        code: 'https://bitbucket.org/ryoboua/personal_website/src/master/',
        Details: () => (
            <p className="text-left" >
                <u>Why I built this:</u> I wanted to showcase my work and didn’t feel that LinkedIn or a resume in PDF format offered an adequate platform.<br/><br/> 
                <u>Challenge:</u> At this point, I can say I’m comfortable building React web apps.
                The challenge was the design aspect - building a smooth, responsive UI/UX experience that translates intelligently from desktop to mobile.<br/><br/> 
                Another challenge was deploying a server that would host and serve my projects - my introduction to DevOps.<br/><br/>
                <u>Major takeaway:</u> Finding a way to serve all my projects was definitly a little challenging as it took me out of my javascript bubble.<br/><br/>
                I had to learn how to use NGINX and how to setup an https connection. I have Cloulflare acting as a proxy to validate my secure connection. 
                The requests are then routed to my DigitalOcean droplet and NGINX delivers the assets of my projects. Furthermore,
                the backend for the notepadapp also uses SSL and the requests are deliver to the server by NGINX via reverse proxy. 
            </p>
        ) 
    },
]

export default class ProjectList extends Component {
    state = {
    expanded: 'panel0',
    }

    handleChange = panel => (e, expanded) => {
    this.setState({
        expanded: expanded ? panel : false,
    })
    }

    render() {
        const { expanded } = this.state
        const { screenWidth } = this.props
        return (
            <div className="text-black" style={{ paddingBottom: '3rem' }}>
            {
                projectList.map( ({ code, url, title, Details }, index) => (
                    <ExpansionPanel 
                        key={index}
                        elevation={0}
                        style={{ backgroundColor: '#282c34', color: 'white' }} 
                        expanded={expanded === `panel${index}`} 
                        onChange={this.handleChange(`panel${index}`)}
                    >
                        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>

                            <h4 style={{ width: '100px', flexBasis: screenWidth > 1023 ? '77.77%' : '66.66%', flexShrink: 0, textAlign: 'left' }} >{title}</h4>
                            
                            <a href={code} target="_blank" rel="noopener noreferrer" style={{  marginRight: '1%' }} >
                                <Chip
                                    style={{ backgroundColor: '#DC3446', color: 'white'}} 
                                    label={ screenWidth > 750 ? 'View Code' : <i className="fas fa-code"></i>}
                                    clickable 
                                />
                            </a>
                            
                            <a href={url} target="_blank" rel="noopener noreferrer" >
                                <Chip 
                                    style={{ backgroundColor: '#E83E0C', color: 'white' }} 
                                    label={ screenWidth > 750 ? 'View Project' : <i className="fas fa-external-link-alt"></i> } 
                                    clickable
                                />
                            </a>

                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Details />
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                ) )
            }
            </div>
        )
    }
}