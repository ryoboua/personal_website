import pic1 from './support_tool1.png'
import pic2 from './support_tool2.png'
import pic3 from './support_tool3.png'
import pic4 from './support_tool4.png'

export default [
    {
        url: pic1,
        alt: 'User tool',
    },
    {
        url: pic2,
        alt: 'Course tool - 1',
    },
    {
        url: pic3,
        alt: 'Course tool - 2',
    },
    {
        url: pic4,
        alt: 'Course tool - 3',
    },

]