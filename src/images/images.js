import portrait from './portrait.jpg'
import programming from './programming.svg'
import frontEnd from './undraw_static_assets.svg'
import backEnd from './undraw_server_status.svg'
import blockChain from './undraw_ethereum.svg'
import devTool from './undraw_building_blocks.svg'
import ToolPics from './support_tool_pics/supportToolPics.js'

export const portraitImage = {
    url: portrait,
    alt: 'Portait of a developer',
}

export const frontEndImage = {
    url: frontEnd,
    alt: 'HTML, CSS and Javacript logos',
}
export const backEndImage = {
    url: backEnd,
    alt: 'Stack of servers',
}
export const blockChainImage = {
        url: blockChain,
        alt: 'Girl holding balloon with Ethereum logo',
}
export const devToolImage = {
    url: devTool,
    alt: 'Guy building a break wall',
}

export const programmingImage = {
    url: programming,
    alt: 'secret pic',
}

export const supportToolPics = ToolPics