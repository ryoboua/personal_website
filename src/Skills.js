import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import { Collapse } from 'react-collapse'

// eslint-disable-next-line
import { frontEndImage, backEndImage, blockChainImage, devToolImage } from './images/images'

const skills = [
  {
    title: 'Frontend',
    image: frontEndImage,
    tech: ['HTML', 'CSS', 'Javascript', 'React', 'Reactstrap', 'Redux', 'Google Material-UI', 'Bootstrap', 'Bulma', 'Handlebars', 'Pug', 'jQuery'],
  },
  {
    title: 'Backend',
    image: backEndImage,
    tech: ['Nodejs', 'Expressjs', 'Mongoosejs', 'JSONWebToken', 'MongoDB', 'Python', 'Flask', 'Django', 'MySQL', 'NGNIX'],

  }, 
//   {
//     title: 'Blockchain',
//     image: blockChainImage,
//     tech: ['Solidity', 'Remix', 'Trufflejs', 'web3js', 'Ganache', 'Ethereum'],

//   },
  {
    title: 'Tools & Services',
    image: devToolImage,
    tech: ['NPM','Chrome Dev Tools', 'Postman', 'Jest', 'Mocha/Chai', 'GitHub/Git', 'Bitbucket', 'Docker', 'PM2', 'Cloudflare', 'OpenSSL', 'Digital Ocean'],
  }
]

class MobileView extends Component {
    state = {
        showSkill: 0,
    }

    toggleSkill = i => this.setState({ showSkill: i })

    render(){
        const { showSkill } = this.state
        return (
            <div className="container mx-auto bg-white" style={{ maxWidth: '1200px' , borderRadius: '12px', paddingTop: '20px' }} >
            {
                skills.map( ({title, image, tech}, i) => (
                    <div key={title} className="w-100" style={{ padding: '10px 45px' }} >
                        <h4 className="pb-3" style={{ textDecoration: 'underline' }} >{title}</h4>
                        <img src={image.url} alt={image.alt}  height="175" width="175" />
                        { 
                        showSkill !== i &&
                            (<div>
                                <Button 
                                    style={{ backgroundColor: '#E83E0C', color: 'white', margin: '1.5em auto' }} 
                                    variant="contained" 
                                    onClick={() => this.toggleSkill(i)} 
                                >
                                    View
                                </Button>
                            </div>)
                        }
                        <Collapse isOpened={ showSkill === i } >
                            {tech.map( t => <p key={t} >{t}</p> )}
                        </Collapse>
                    </div>
                ))
            }
            </div>
         )
    }
}

const DesktopView = () => (
    <div className="container mx-auto bg-white pt-1" style={{ maxWidth: '1200px' , borderRadius: '12px' }} >
        {
            skills.map( ({title, image, tech}) => (
                <div key={title} className="d-flex w-100 text-center" >
                    <div className="w-25" style={{ padding: '1em 1em' }} >
                        <h4 className="mx-auto" style={{ textDecoration: 'underline' }} >{title}</h4>
                        <img src={image.url} alt={image.alt}  height="165" width="165" />    
                    </div>
                    <div className="w-75 text-left" style={{ padding: '7em 2em 0em' }} >
                        {tech.map( (t , i) => <p key={t} style={{ fontSize: '1.2em' }} className="d-inline-block m-1">{t}{tech.length - 1 !== i ? ',': undefined}</p> )}
                    </div>
                </div>
            ) )
        }
    </div>
)

const Skills = ({ screenWidth }) =>  screenWidth < 760 ? <MobileView /> : <DesktopView />

export default Skills