import React from 'react'

export const introText = `A web developer looking to join an exciting company with experienced developers and room for growth.
                          I’m reliable and effective, always seeking to learn and stand out from the norm.
                          On this site, you can see my progression and what I can do as a web developer today.`

export const LittleAboutMe = ({ screenWidth }) => (
    <p className="pt-4"style={{ textAlign: 'left', margin: screenWidth > 999 ? '0 25.6%' : '0 1.5rem'}} >
      In the summer of 2018, I took the risk of leaving a technical support job to pursue my interest in coding full time.
      Overnight, my routine changed completely - instead of getting up and going to an office, I woke up at 9 a.m. every day to code on my own.
      To make sure I was learning everything I needed to know,
      I went to hackathons like TD Elevate , <a href="https://medium.com/shyft-network-media/winners-announced-shyfthack-blockchain-hackathon-45a1fb4dbd84" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >
      Shyft</a>  and <a href="https://www.linkedin.com/feed/update/urn:li:activity:6417060441276633088" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >Nobul</a> and 
      developed connections in the Toronto coding community by going to meetups. I recently won the <a href="http://giftthecode.ca/" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >Gift The Code </a> hackathon hosted by Capital One -
      Check out this article on the hackathon.<br/><br/>
  
      When I learned about a new language or concept, I would pursue it until I understood it fully. I assigned myself projects to test and demonstrate my skills.
      My friends said things like “wow you’re so disciplined” and “hey you okay? I haven't heard from you in a while”, but I had a goal and could not be distracted.
      Three months later, I’m sharing what I’ve created in order to forge new connections and identify opportunities in my chosen field.<br/>
    </p>
    )
  
export const MoreAboutMe = ({ screenWidth }) => (
  <p className="pt-4"style={{ textAlign: 'left', margin: screenWidth > 999 ? '0 25.6%' : '0 1.5rem'}} >
    Below is a list of my tech stack, which includes technologies that I’ve used to build this site and my other projects. As any web developer knows,
    you need to familiarize yourself with different frameworks and tools because there are a several ways to build an application.
    It's part of the reason why I chose to learn Javascript. Switching context from frontend (React) to backend (Nodejs) is all using Javascript so it made learning a lot easier for me.
    Early on, I started to learn the fundamentals of Javascript from codecademy, w3schools, MDN web docs and
    youtube channel <a href="https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q/featured" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >Fun Fun Function</a> (Shoutout to MPJ).
    <br/><br/>

    I taught myself React because it's one of the highest frontend frameworks in demand.
    By watching numerous video tutorials on YouTube and taking free courses online as well as reading Medium articles and the official React documentation, I was able to start <i>Thinking in React</i>.
    However, the most effective resource for me was the <a href="https://www.fullstackreact.com/" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >fullstackreact</a> book by fullstack.io. 
    The book helped me quickly grasp the fundamental concepts of React such as state, props, children, component lifecycle method, the virtual DOM, routing and create react app. 
    As well as Redux, some best practices to follow and new features like the Context API. 
    I’ve been to numerous hackathons where my teammates will ask for resources to learn React and I always recommend the book.<br/><br/>

    For learning Nodejs, building a REST API and a fullstack app, I’ve done numerous courses from Udemy
    , <a href="https://www.youtube.com/user/TechGuyWeb" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >Traversy Media</a>
    , <a href="https://wesbos.com/" target="_blank" rel="noopener noreferrer" style={{ color: '#303F9F' }} >Wes Bos</a>. Once I got better at reading code,
    I started looking at other projects on GitHub and going through the file structure and trying to understand and dissect how other developers implemented their app.
    As a result, I would become familiar with different node packages.
    </p>
)  