import React, { Component } from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button'
import ImageLightBox from './ImageLightBox'

export const uOttawaDescription = (
    <p>
      Growing up, I was very curious and liked to take everything apart - my parents hated that.<br /><br />
      My electrical engineering degree helped strengthen an intuition for understanding technical systems, which carried me into my journey to becoming a self-taught developer.
      Through co-op terms and group projects, I developed my professionalism, disciplined work ethic, and strong social skills.
    </p>
  )
  
export const IBMDescription = (
    <p>
      In my Technical Analyst position, I provided assistance and support to clients in regards to the Cognos BI application. 
      I conducted research on error messages, analyzed log files, and provided solutions to the affected user. 
      I attribute the growth of my troubleshooting skills to this position.
    </p>
  )
  
export const CBSADescription = (
    <p>
      Initially, my role in this position was to conduct manual regression tests on a software to be used by border security agents.
      After offering to take on more challenging work, I was given the task of writing unit tests in Java.
      This was the beginning of my interest in programming.
    </p>
  )
  
export const PrivyDescription = (
    <p>
      I received a 11 month contract with the Privy Council Office as part of a desktop modernization project.
      My team was responsible for upgrading the computers used by the Privy Council from Windows XP to Windows 7 and provide general technical support.
      I obtained extremely high security clearance and was required to follow strict protocols in order to work in this sensitive environment.
    </p>
  )
  
export const CIBCDescription = (
    <p>
      After graduating from the University of Ottawa, I moved back to my hometown of Toronto and took on the challenge of finding work in my field.
      After finding that pure electrical engineering jobs were highly competitive due to lack of experience, I decided to leverage my bilingualism and customer service skills gained through co-op positions to take a job at CIBC.
      Throughout this process, I discovered the importance of networking and standing out from the crowd - I realized that I had to upgrade my skills, become a self-driven learner, and find unique ways to showcase myself.
    </p>
  )

export const presentDescription = (
<p>
    Hey you've made it to the end of my website! Thanks for taking the time to go through it!<br/><br/>

    In the present day, you can bet I'm either in my room or at a coffeeshop coding and learning about new emerging technologies such as blockchain and AI.
</p>
)

export class TopHatDescription  extends Component {

    state = { showTopHatAccomplishment: false, showImageLightBox: false  }
    
    toggleTopHatAccomplishment = () => this.setState({ showTopHatAccomplishment: !this.state.showTopHatAccomplishment })
    toggleShowImageLightBox = () => this.setState({ showImageLightBox: !this.state.showImageLightBox, showTopHatAccomplishment: false })

    render() {
        return (
        <div>
            <p>
                In Tophat’s fast-paced tech environment, I was required to take on a variety of tasks, be flexible and efficient, and teach myself new skills whenever required.
            </p>
            <br />
            <div className="w-100 text-center" >
                <Button variant="raised" color="primary"  onClick={this.toggleTopHatAccomplishment}>
                    My favorite accomplishment...
                </Button>
            </div>
            <br />
            <p>
                Some of my accomplishments include:
            </p>
            <TopHatAccomplishment 
                showTopHatAccomplishment={this.state.showTopHatAccomplishment} 
                toggleTopHatAccomplishment={this.toggleTopHatAccomplishment}
                toggleShowImageLightBox={this.toggleShowImageLightBox}
            />
            <ImageLightBox isOpen={this.state.showImageLightBox} toggleShowImageLightBox={this.toggleShowImageLightBox} />        
            <ul>
                <li>Developed and project managed tools (in Python and Django) that are used to accomplish tasks internally with ease and simplicity while maintaining system security and integrity.</li>
                <li>Supported Top Hat's integration with universities’ Single Sign On (SSO) and Learning Management Systems (LMS) including Blackboard, Canvas, D2L, Moodle, and Sakai.</li>
                <li>Drove the technical conversation in Enterprise meetings to advocate for Top Hat implementation.</li>
                <li>Reported bugs and feature requests to the Product and Engineering team and followed up with customers upon resolution or implementation.</li>
                <li>Created weekly custom reports for professors on class analytics.</li>
                <li>Automated student and professor account creation for Top Hat Certification Program.</li>
                <li>Served as technical advisor and on-site support for the Indiana University tablet pilot project.</li>
                <li>Gained practical knowledge about relational databases and SQL querying language.</li>
                <li>Became fluent in Salesforce Administrator features such as creating and editing cases, custom reports, dashboards and macros.</li>
                <li>Assisted with the training of new employees.</li>
            </ul>
        </div>
        )
    }
}

const TopHatAccomplishment = ({ showTopHatAccomplishment, toggleTopHatAccomplishment, toggleShowImageLightBox }) => {
    return (
        <Dialog
            open={showTopHatAccomplishment}
            onClose={toggleTopHatAccomplishment}
        >
        <DialogTitle>Accomplishment</DialogTitle>
        <DialogContent>
            <p style={{ color: 'black' }}>
                My favorite accomplishment at Top Hat was before leaving, a colleague on my team and I built an internal tool used by technical support analyst to support students and professors.
                <br /><br />
        
                Supporting Top Hat required a lot of different applications and over time things would get overwhelming especially 
                if you had a professor on hold waiting for an issue to be resolved. The backend of Top Hat is built using Django. We built a custom Django admin panel view from which
                agents are able to complete certain tasks with one click rather than 4 clicks and using 2 different applications.<br /><br />
        
                I gained a lot of project management experience while building this tool because in order to get it done,
                we needed a certain level of access which meant some executives of the company had to approve the project.
                Before even writing a single line of code, we had to present mocks of the application and a business case on
                why our department needed the application and how it would improve the metrics the technical support analyst performance were measured on.<br /><br />
        
                Once we got approval, we were given one week to build the application when originally asking for a month. At the time of starting the project I knew very little python but I had been writing a lot of javascript for
                personal projects so I was able to pick it up quickly.<br /><br /> 
                
                At the end of the week we were able to get a finished version of the app that was approved by Engineering.<br /><br />
                Below are screenshots of the tool.
            </p>
            <div className="text-center">
                <Button type="Button" variant="contained" color="secondary" onClick={toggleShowImageLightBox} >
                View pictures
                </Button>
            </div>

        </DialogContent>
        <DialogActions>
            <Button  color="primary" autoFocus onClick={toggleTopHatAccomplishment} >
            Close
            </Button>
        </DialogActions>
        </Dialog>
    )
}