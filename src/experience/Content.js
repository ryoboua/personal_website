import React from 'react'
import Divider from '@material-ui/core/Divider'

export default ({ position, period, location, description }) => (
    <div>
      <h6>{position}</h6>
      <h6>{period}</h6>
      <h6>{location}</h6>
      <Divider style={{margin: '1.4% 0px'}}  />
      {description}
    </div>
)