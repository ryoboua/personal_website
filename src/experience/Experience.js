import React, { Component } from 'react'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepContent from '@material-ui/core/StepContent'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'

import Content from './Content'
import { 
  uOttawaDescription, 
  IBMDescription, 
  CBSADescription, 
  PrivyDescription, 
  CIBCDescription, 
  TopHatDescription, 
  presentDescription
} from './description'

const steps = [
    {
      title: 'University of Ottawa',
      content: <Content 
                  position="BSc Electrical Engineering" 
                  period="September 2010 - April 2015" 
                  location="Ottawa, On" 
                  description={uOttawaDescription} 
              />,
    },
    {
      title: 'IBM Cognos',
      content: <Content 
                  position="Technical Analyst" 
                  period="January 2013 - April 2013 • 4 months" 
                  location="Ottawa, On" 
                  description={IBMDescription} 
                />,
    },
    {
      title: 'Canada Border Services Agency',
      content: <Content 
                  position="Software QA Tester" 
                  period="September 2013 - December 2013 • 4 months" 
                  location="Ottawa, On" 
                  description={CBSADescription} 
                />,
    },
    {
      title: 'Privy Council Office',
      content: <Content 
                  position="Technical Support Officer" 
                  period="May 2014 - March 2015 • 11 months" 
                  location=" Ottawa, On" 
                  description={PrivyDescription} 
                />,
    },
    {
      title: 'CIBC',
      content: <Content 
                    position="Bilingual Help Desk Specialist" 
                    period="October 2015 - November 2016 • 1 year 2 months" 
                    location="Toronto, On" 
                    description={CIBCDescription} 
                  />,
    },
    {
      title: 'Top Hat',
      content: <Content 
                    position="Senior Implementation Specialist &#x26; Technical Analyst" 
                    period="December 2016 - July 2018 • 1 year 8 months" 
                    location="Toronto, On" 
                    description={<TopHatDescription/>} 
                  />,
    },
    {
      title: 'Present',
      content: <Content 
                  position="Personal Hypeman" 
                  period="August 2018 - Present" 
                  location="Toronto, On" 
                  description={presentDescription} 
                />,
    },
]

export default class Experience extends Component {

  state = { activeStep: 0 }
  
  handleNext = () => this.setState({ activeStep: this.state.activeStep + 1 })
  handleBack = () => this.setState({ activeStep: this.state.activeStep - 1 })
  handleReset = () => this.setState({ activeStep: 0 })
  skipToStep = i => this.setState({ activeStep: i })
  
  render() {
    const { activeStep } = this.state
    const { screenWidth } = this.props
    return (
      <div className="container mx-auto bg-danger" style={{ maxWidth: '1200px', borderRadius: '12px', marginTop: '25px' }} >
        <div style={{backgroundColor: '#DC3446', padding: screenWidth > 800 ? '50px 45px' : '0' , textAlign: 'left'}}>
          <Stepper style={{backgroundColor: '#DC3446', color: 'white', padding: '24px 0px'}} activeStep={activeStep} orientation="vertical">
            {steps.map((step, index) => {
              return (
                <Step key={step.title} >
                    <h4
                      alternativelabel="true"
                      active="false" 
                      completed="false"
                      last="false"
                      className="experience-title" 
                      onClick={() => this.skipToStep(index)}  
                      style={{ color: activeStep === index ? 'white': 'black'}} 
                    >
                      {step.title}
                    </h4>

                  <StepContent>
                    {step.content}
                    <div style={{ marginBottom: '16px'}}>
                      <div>
                        <Button
                          disabled={activeStep === 0}
                          onClick={this.handleBack}
                          style={{
                              color: 'white'
                          }}
                        >
                          Back
                        </Button>
                        <Button
                          variant="contained"
                          onClick={this.handleNext}
                          style={{  
                            backgroundColor: '#E83E0C',
                            color: 'white'
                          }}
                        >
                          {index === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button>
                      </div>
                    </div>
                  </StepContent>
                </Step>
              )
            })}
          </Stepper>
          {activeStep === steps.length &&
            <Paper style={{backgroundColor: '#DC3446', padding: '24px'}}  square elevation={0}>
              <p>
                Why did the can crusher quit his job?<br/><br/>

                Because it was soda pressing.
              </p>
              <Button style={{backgroundColor: '#E83E0C', color: 'white'}} variant="contained" onClick={this.handleReset} >
                Reset
              </Button>
            </Paper>
          }
        </div>
      </div>
    )
  }
}