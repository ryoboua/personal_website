import React, { Component } from 'react'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'

import { supportToolPics } from '../images/images.js'

export default class ImageLightBox extends Component {
    
    state = { photoIndex: 0 }

    render() {
        const { photoIndex } = this.state;
        const { isOpen } = this.props

        return (
        <div>
        {isOpen && (
            <Lightbox
            mainSrc={supportToolPics[photoIndex].url}
            nextSrc={supportToolPics[(photoIndex + 1) % supportToolPics.length].url}
            prevSrc={supportToolPics[(photoIndex + supportToolPics.length - 1) % supportToolPics.length].url}
            onCloseRequest={this.props.toggleShowImageLightBox}
            onMovePrevRequest={() =>
                this.setState({
                photoIndex: (photoIndex + supportToolPics.length - 1) % supportToolPics.length,
                })
            }
            onMoveNextRequest={() =>
                this.setState({
                photoIndex: (photoIndex + 1) % supportToolPics.length,
                })
            }
            />
        )}
        </div>
        )
    }
}