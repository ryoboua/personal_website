import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import { Collapse } from 'react-collapse'
import Skills from './Skills'
import ProjectList from './ProjectList'
import Experience from './experience/Experience'
import { introText, LittleAboutMe, MoreAboutMe } from './AboutMe'
import { portraitImage ,programmingImage } from './images/images'

class App extends Component {

  state = {
    screenWidth: null,
    showMoreAboutMe: false,
  }

  componentDidMount(){
        //Screen width
        this.updateWindowWidth()
        window.addEventListener('resize', this.updateWindowWidth);
  }

  updateWindowWidth = () => this.setState({ screenWidth: window.innerWidth })

  toggleShowMoreAboutMe = () => this.setState({ showMoreAboutMe: !this.state.showMoreAboutMe })

  render() {
    const { screenWidth, showMoreAboutMe } = this.state
    return (
      <main style={{ backgroundColor:"#282c34" }}>

          <section className="text-left text-white" style={{ margin: screenWidth > 999 ? '5.6% 25.6% 0' : '5.6% 1.5rem 0'}} >
              <h1>Reginald Yoboua.</h1>
              <h1>Fullstack Javascript Developer.</h1>
              <h1>Electrical Engineer.</h1>
              <h5 className="pt-3">Email: whoisreggie.ca@gmail.com</h5>
              <p className="pt-4" >{introText}</p>
              <div className="text-center mb-4" >
                <img src={portraitImage.url} alt={portraitImage.alt} width="226" height="302" style={{ borderRadius: '12px' }} />
              </div>
          </section>

          <section className="bg-danger text-center pt-5" style={{ paddingBottom: '23rem' }} >
              <h3>A little about myself.</h3>

              <LittleAboutMe screenWidth={screenWidth} />

              <Collapse isOpened={showMoreAboutMe} >
                <MoreAboutMe screenWidth={screenWidth} />
              </Collapse>

              <Button 
                  style={{ margin: '1.5em auto' }} 
                  variant="raised" 
                  color="primary" 
                  onClick={this.toggleShowMoreAboutMe} 
              >
                  {showMoreAboutMe ? 'show less about me' : 'show more about me'}
              </Button>
              
          </section>

          <section style={{ margin: '-22rem 1.5rem 0', textAlign: 'center', }} >
            <Skills screenWidth={screenWidth} />
          </section>

          <section className="text-center text-white mx-auto" style={{ marginTop: '4rem',maxWidth: '1200px', paddingBottom: '15rem'}} >
            <h3>My Projects</h3>
            <p>Here are a few recent projects</p>
            <ProjectList screenWidth={screenWidth} />
          </section>

          <section style={{ margin: '-14rem 1.5rem 0', textAlign: 'center', color: 'white' }} >
            <h3>Experience</h3>
            <p>Here is an interactive journey of my experience</p>
            <Experience screenWidth={screenWidth} />
          </section>

          <footer className="text-center text-white" style={{margin: '32.5px 0px 25px 0px'}} >
            <img src={programmingImage.url} alt={programmingImage.alt} height="137.5" width="137.5" />
            <p>Site designed &#x26; coded by me in 2018</p>
            <p>Images provided by <a href="https://undraw.co/" >undraw.co</a></p>
            <p>Site inspired by <a href="http://mattfarley.ca/" >mattfarley.ca</a></p>
            <div style={{ height: '50px', fontSize: '2em' }} >
              <a href="https://github.com/ryoboua" style={{ color: 'white'}} ><i className="fab fa-github"></i></a>{' '}
              <a href="https://bitbucket.org/ryoboua/" style={{ color: 'white'}} ><i className="fab fa-bitbucket"></i></a>{' '}
              <a href="https://www.linkedin.com/in/ryoboua/" style={{ color: 'white'}} ><i className="fab fa-linkedin"></i></a>{' '}
            </div>
          </footer>

        </main>
    );
  }
}

export default App